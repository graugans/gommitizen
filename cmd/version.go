/*
SPDX-License-Identifier: MIT
Copyright © 2022 Christian Ege <ch@ege.io>

*/

package cmd

import (
	"fmt"
	"runtime"

	"github.com/spf13/cobra"
)

// versionCmd represents the version command
var (
	shortened  = false
	Version    = "0.0.0"
	Commit     = "none"
	BuildDate  = "unknown"
	BuiltBy    = "myself"
	versionCmd = &cobra.Command{
		Use:   "version",
		Short: "Version will output the current build information",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			if shortened {
				fmt.Printf("%s\n", Version)
			} else {
				fmt.Printf("version: %s\n", Version)
				fmt.Printf("commit: %s\n", Commit)
				fmt.Printf("date: %s\n", BuildDate)
				fmt.Printf("builtBy: %s\n", BuiltBy)
				fmt.Printf("OS architecture: %s\n", gzOSARCH)
			}
		},
	}
)

const (
	gzOSARCH = runtime.GOOS + "_" + runtime.GOARCH
)

func init() {
	versionCmd.Flags().BoolVarP(&shortened, "short", "s", false, "Use shortened output for version information.")
	rootCmd.AddCommand(versionCmd)
}
