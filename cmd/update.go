/*
SPDX-License-Identifier: MIT
Copyright © 2022 Christian Ege <ch@ege.io>

*/

package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"sort"

	"github.com/Masterminds/semver"
	"github.com/spf13/cobra"
)

// Holds the information about the author of an release.
type ReleaseAuthor struct {
	Id       int    `json:"id"`       // The GitLab user ID of the release author
	Username string `json:"username"` // The GitLab user name
	Name     string `json:"name"`     // The Name of the GitLab user
}

// Holds the Link to a specific asset item
type ReleaseLink struct {
	Id             int    `json:"id"`               // The ID of the asset
	Name           string `json:"name"`             // The name of the asset
	Url            string `json:"url"`              // The asset URL
	DirectAssetUrl string `json:"direct_asset_url"` // The direct asset URL
	External       bool   `json:"external"`         // Holds the information if this asset an external asset
	LinkType       string `json:"link_type"`        // Holds the information about the link type
}

// Holds the releas assets
type ReleaseAssets struct {
	Count uint          `json:"count"` // The number of elements in this object
	Links []ReleaseLink `json:"links"` // A slice of the ReleaseLink structures
}

type Releases struct {
	Name       string        `json:"name"`
	CreatedAt  string        `json:"created_at"`
	ReleasedAt string        `json:"released_at"`
	TagPath    string        `json:"tag_path"`
	Author     ReleaseAuthor `json:"author"`
	Assets     ReleaseAssets `json:"assets"`
}

func getJson(url string, target interface{}) error {
	r, err := httpClient.Get(url)
	if err != nil {
		fmt.Printf("Some error occurred! \n")
		return err
	}
	defer r.Body.Close()
	return json.NewDecoder(r.Body).Decode(target)
}

func convertReleasesToSemVerVersions(releases *[]Releases) ([]*semver.Version, error) {
	vs := make([]*semver.Version, len(*releases))
	for i, r := range *releases {
		v, err := semver.NewVersion(r.Name)
		if err != nil {
			return []*semver.Version{}, errors.New(fmt.Sprintf("Error parsing version: %s", err))
		}
		vs[i] = v
	}
	sort.Sort(semver.Collection(vs))
	return vs, nil
}

func getMaxVersion(versions []*semver.Version) *semver.Version {
	maxVersion, err := semver.NewVersion("0.0.0")
	if err == nil && len(versions) > 0 {
		return versions[len(versions)-1]
	}
	return maxVersion
}

// updateCmd represents the update command line option
var (
	httpClient = http.Client{
		CheckRedirect: func(r *http.Request, via []*http.Request) error {
			r.URL.Opaque = r.URL.Path
			return nil
		},
	}
	updateCmd = &cobra.Command{
		Use:   "update",
		Short: "Perform an update of this application",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("update called")
			// example
			// https://gitlab.com/graugans/gommitizen/-/releases/v0.0.2/downloads/gommitizen_0.0.2_Linux_x86_64.tar.gz
			var releases []Releases
			err := getJson(updateQueryURL, &releases)
			if err != nil {
				fmt.Printf("Some error occured! %s \n", err)
			}
			fmt.Printf("Current version: %s\n", Version)
			fmt.Printf("OS architecture: %s\n", gzOSARCH)
			fmt.Printf("Releases: %d\n", len(releases))

			vs, err := convertReleasesToSemVerVersions(&releases)
			maxVersion := getMaxVersion(vs)
			fmt.Printf("Max Version: %s\n", maxVersion)
			currentVersion, err := semver.NewVersion(Version)
			localBuildVersion, err := semver.NewVersion("0.0.0")
			if currentVersion.Equal(localBuildVersion) {
				fmt.Printf("Local builds are not supported!\n")
				return
			} else {
				releaseName := fmt.Sprintf("gommitizen_%s_%s.tar.gz", maxVersion.String(), gzOSARCH)
				fmt.Printf("We are looking for %s\n", releaseName)
				if currentVersion.LessThan(maxVersion) {
					fmt.Printf("An update is available: %s\n", maxVersion)
					for i, r := range releases {
						if r.Name == maxVersion.Original() {
							for _, l := range releases[i].Assets.Links {
								if l.Name == releaseName {
									fmt.Printf("Download URL: %s\n", l.Url)
								}
							}
						}
					}
				}
			}
		},
	}
)

const (
	updateQueryURL = "https://gitlab.com/api/v4/projects/35593891/releases"
)

func init() {
	rootCmd.AddCommand(updateCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// updateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// updateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
