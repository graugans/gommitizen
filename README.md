# gommitizen

A Commitizen clone in Go. Some toy project to teach me Go

## Installation

### Unix like systems

Ensure that `~/bin` is in your `PATH` variable. Now you can downlad and extract the binary

#### Linux x86-64

```
curl -sL https://gitlab.com/graugans/gommitizen/-/releases/v0.0.5/downloads/gommitizen_0.0.5_Linux_x86_64.tar.gz | tar -xzv -C ~/bin gz
```

## This is a work in progress

In case you are looking for a turn-key solution, this is not yet in a usable shape

## License
This software is licensed under the terms of the [MIT License](LICENSE)

## Project status
This is my little toy project to teach myself programming Go. There is a high propability that it will never turn into any uselful.