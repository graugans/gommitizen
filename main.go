/*
SPDX-License-Identifier: MIT
Copyright © 2022 Christian Ege <ch@ege.io>

*/

package main

import "gitlab.com/graugans/gommitizen/cmd"

func main() {
	cmd.Execute()
}
